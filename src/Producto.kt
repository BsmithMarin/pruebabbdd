package src

class Producto {
    var IDProducto:Int?=null
    var Nombre:String?=null
    var TipoIVA:Double?=null
    var CaducidadMEdiaDias:Int?=null

    constructor(idp:Int,nom:String,TIV:Double,CMD:Int){
        IDProducto=idp
        Nombre=nom
        TipoIVA=TIV
        CaducidadMEdiaDias=CMD
    }
}